export const environment = {
  production: true,
  apiTrainers: "https://omgekb-noroff-assignment4.herokuapp.com/trainers",
  apiPokemons: "https://pokeapi.co/api/v2/pokemon/?offset=0&limit=151",
  apiPictures: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/",
  apiKey: "",
};
